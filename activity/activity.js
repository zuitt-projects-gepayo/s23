/*
	Activity:

	>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session23)

//Create
	>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you. 

//Read

	>> Find all regular/non-admin users.

//Update

	>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.

//Delete

	>> Delete all inactive courses

//Add all of your query/commands here in activity.js
*/

db.users.insertMany([
    {
        "firstName" : "Recca",
        "lastName" : "Habanishi",
        "email" : "HabanishiRecca@gmail.com",
        "password" : "password123",
        "isadmin" : false
    },

    {
        "firstName" : "Max",
        "lastName" : "Domon",
        "email" : "DomonMax@gmail.com",
        "password" : "password123",
        "isadmin" : false
    },

    {
        "firstName" : "Dylan",
        "lastName" : "Mikagami",
        "email" : "MikagamiDylan@gmail.com",
        "password" : "password123",
        "isadmin" : false
    },

    {
        "firstName" : "Aira",
        "lastName" : "Koganei",
        "email" : "KoganeiLorcan@gmail.com",
        "password" : "password123",
        "isadmin" : false
    },

    {
        "firstName" : "Aira",
        "lastName" : "Kirisawa",
        "email" : "KirisawaAira@gmail.com",
        "password" : "password123",
        "isadmin" : false
    }

]);

db.courses.insert([
{
        "name" : "Java101",
        "price" : 2500,
        "isActive" : false
},

{
    "name" : "HTML111",
    "price" : 1500,
    "isActive" : false
},

{
    "name" : "JavaScript222",
    "price" : 2000,
    "isActive" : false
}
]);

db.users.updateOne({"firstName" : "Recca"}, {$set: {"isadmin" : true}})

db.courses.updateOne({"name" : "HTML111"}, {$set: {"isActive" : true}})

db.courses.deleteMany({"isActive" : false})